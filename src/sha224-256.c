/************************* sha224-256.c ************************/
/***************** See RFC 6234 for details. *******************/
/*
 Copyright 2024 Fabrice Ferino
 SPDX-License-Identifier: Apache-2.0
 */

/* Copyright (c) 2011 IETF Trust and the persons identified as */
/* authors of the code.  All rights reserved.                  */
/* See sha.h for terms of use and redistribution.              */

/*
 * Description:
 *   This file implements the Secure Hash Algorithms SHA-224 and
 *   SHA-256 as defined in the U.S. National Institute of Standards
 *   and Technology Federal Information Processing Standards
 *   Publication (FIPS PUB) 180-3 published in October 2008
 *   and formerly defined in its predecessors, FIPS PUB 180-1
 *   and FIP PUB 180-2.
 *
 *   A combined document showing all algorithms is available at
 *       http://csrc.nist.gov/publications/fips/
 *              fips180-3/fips180-3_final.pdf
 *
 *   The SHA-224 and SHA-256 algorithms produce 224-bit and 256-bit
 *   message digests for a given data stream.  It should take about
 *   2**n steps to find a message with the same digest as a given
 *   message and 2**(n/2) to find any two messages with the same
 *   digest, when n is the digest size in bits.  Therefore, this
 *   algorithm can serve as a means of providing a
 *   "fingerprint" for a message.
 *
 * Portability Issues:
 *   SHA-224 and SHA-256 are defined in terms of 32-bit "words".
 *   This code uses <stdint.h> (included via "sha.h") to define 32-
 *   and 8-bit unsigned integer types.  If your C compiler does not
 *   support 32-bit unsigned integers, this code is not
 *   appropriate.
 *
 * Caveats:
 *   SHA-224 and SHA-256 are designed to work with messages less
 *   than 2^64 bits long.  This implementation uses SHA224/256Input()
 *   to hash the bits that are a multiple of the size of an 8-bit
 *   octet, and then optionally uses SHA224/256FinalBits()
 *   to hash the final few bits of the input.
 */

#include "sha.h"
#include "sha-private.h"

/* Define the SHA shift, rotate left, and rotate right macros */
#define SHA256_SHR(bits,word)      ((word) >> (bits))
#define SHA256_ROTL(bits,word)                         \
(((word) << (bits)) | ((word) >> (32-(bits))))
#define SHA256_ROTR(bits,word)                         \
(((word) >> (bits)) | ((word) << (32-(bits))))

/* Define the SHA SIGMA and sigma macros */
#define SHA256_SIGMA0(word)   \
(SHA256_ROTR( 2,word) ^ SHA256_ROTR(13,word) ^ SHA256_ROTR(22,word))
#define SHA256_SIGMA1(word)   \
(SHA256_ROTR( 6,word) ^ SHA256_ROTR(11,word) ^ SHA256_ROTR(25,word))
#define SHA256_sigma0(word)   \
(SHA256_ROTR( 7,word) ^ SHA256_ROTR(18,word) ^ SHA256_SHR( 3,word))
#define SHA256_sigma1(word)   \
(SHA256_ROTR(17,word) ^ SHA256_ROTR(19,word) ^ SHA256_SHR(10,word))

/*
 * Add "length" to the length.
 * Set Corrupted when overflow has occurred.
 */

static int SHA224_256AddLength(SHA256Context *context, uint32_t length)
{
    uint32_t carry;
    context->length_low += length;
    carry = (context->length_low < length) ? 1 : 0;
    context->length_high += carry;
    return (context->length_high < carry) ? shaInputTooLong : shaSuccess;
}

/* Local Function Prototypes */
static int SHA224_256Reset(SHA256Context *context, uint32_t *H0);
static void SHA224_256Transform(SHA256Context *context,
                                const uint8_t buff[/* block_size */]);
static void SHA224_256Finalize(SHA256Context *context,
                               uint8_t Pad_Byte);
static void SHA224_256PadMessage(SHA256Context *context,
                                 uint8_t Pad_Byte);
static int SHA224_256ResultN(SHA256Context *context,
                             uint8_t Message_Digest[ ], int HashSize);

/* Initial Hash Values: FIPS 180-3 section 5.3.2 */
static uint32_t SHA224_H0[SHA256HashSize/4] = {
    0xC1059ED8, 0x367CD507, 0x3070DD17, 0xF70E5939,
    0xFFC00B31, 0x68581511, 0x64F98FA7, 0xBEFA4FA4
};

/* Initial Hash Values: FIPS 180-3 section 5.3.3 */
static uint32_t SHA256_H0[SHA256HashSize/4] = {
    0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
    0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19
};

/*
 * SHA224Reset
 *
 * Description:
 *   This function will initialize the SHA224Context in preparation
 *   for computing a new SHA224 message digest.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to reset.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA224Reset(SHA224Context *context)
{
    return SHA224_256Reset(context, SHA224_H0);
}

/*
 * SHA224Input
 *
 * Description:
 *   This function accepts an array of octets as the next portion
 *   of the message.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *   message_array[ ]: [in]
 *     An array of octets representing the next portion of
 *     the message.
 *   length: [in]
 *     The length of the message in message_array.
 *
 * Returns:
 *   sha Error Code.
 *
 */
int SHA224Input(SHA224Context *context, const uint8_t *message_array,
                unsigned int length)
{
    return SHA256Input(context, message_array, length);
}

/*
 * SHA224FinalBits
 *
 * Description:
 *   This function will add in any final bits of the message.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *   message_bits: [in]
 *     The final bits of the message, in the upper portion of the
 *     byte.  (Use 0b###00000 instead of 0b00000### to input the
 *     three bits ###.)
 *   length: [in]
 *     The number of bits in message_bits, between 1 and 7.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA224FinalBits(SHA224Context *context,
                    uint8_t message_bits, unsigned int length)
{
    return SHA256FinalBits(context, message_bits, length);
}

/*
 * SHA224Result
 *
 * Description:
 *   This function will return the 224-bit message digest
 *   into the Message_Digest array provided by the caller.
 *   NOTE:
 *    The first octet of hash is stored in the element with index 0,
 *    the last octet of hash in the element with index 27.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to use to calculate the SHA hash.
 *   Message_Digest[ ]: [out]
 *     Where the digest is returned.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA224Result(SHA224Context *context,
                 uint8_t Message_Digest[SHA224HashSize])
{
    return SHA224_256ResultN(context, Message_Digest, SHA224HashSize);
}

/*
 * SHA256Reset
 *
 * Description:
 *   This function will initialize the SHA256Context in preparation
 *   for computing a new SHA256 message digest.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to reset.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA256Reset(SHA256Context *context)
{
    return SHA224_256Reset(context, SHA256_H0);
}

/*
 * SHA256Input
 *
 * Description:
 *   This function accepts an array of octets as the next portion
 *   of the message.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *   message_array[ ]: [in]
 *     An array of octets representing the next portion of
 *     the message.
 *   length: [in]
 *     The length of the message in message_array.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA256Input(SHA256Context *context, const uint8_t *message_array,
                uint32_t length)
{
    uint32_t len = length;

    if (!context) return shaNull;
    if (!length) return shaSuccess;
    if (!message_array) return shaNull;

    while (context->message_block_index < sizeof(context->message_block) && len)
    {
        context->message_block[context->message_block_index] = *message_array++;
        len -= 1;
        context->message_block_index += 1;
    }
    
    if (context->message_block_index == sizeof(context->message_block))
    {
        SHA224_256Transform(context, context->message_block);
    }

    while (len >= SHA256_message_block_size)
    {
        SHA224_256Transform(context, message_array);
        message_array += SHA256_message_block_size;
        len -= SHA256_message_block_size;
    }

    if (len)
    {
        MEMCPY(context->message_block, message_array, len);
        context->message_block_index = len;
    }

    return SHA224_256AddLength(context, length * 8);

}

/*
 * SHA256FinalBits
 *
 * Description:
 *   This function will add in any final bits of the message.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *   message_bits: [in]
 *     The final bits of the message, in the upper portion of the
 *     byte.  (Use 0b###00000 instead of 0b00000### to input the
 *     three bits ###.)
 *   length: [in]
 *     The number of bits in message_bits, between 1 and 7.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA256FinalBits(SHA256Context *context,
                    uint8_t message_bits, unsigned int length)
{
    static uint8_t masks[8] = {
        /* 0 0b00000000 */ 0x00, /* 1 0b10000000 */ 0x80,
        /* 2 0b11000000 */ 0xC0, /* 3 0b11100000 */ 0xE0,
        /* 4 0b11110000 */ 0xF0, /* 5 0b11111000 */ 0xF8,
        /* 6 0b11111100 */ 0xFC, /* 7 0b11111110 */ 0xFE
    };
    static uint8_t markbit[8] = {
        /* 0 0b10000000 */ 0x80, /* 1 0b01000000 */ 0x40,
        /* 2 0b00100000 */ 0x20, /* 3 0b00010000 */ 0x10,
        /* 4 0b00001000 */ 0x08, /* 5 0b00000100 */ 0x04,
        /* 6 0b00000010 */ 0x02, /* 7 0b00000001 */ 0x01
    };
    int retVal;
    if (!context) return shaNull;
    if (!length) return shaSuccess;
    if (length >= 8) return shaBadParam;

    if ((retVal = SHA224_256AddLength(context, length)) != shaSuccess)
    {
        return retVal;
    }

    SHA224_256Finalize(context, (uint8_t)
                       ((message_bits & masks[length]) | markbit[length]));

    return shaSuccess;
}

/*
 * SHA256Result
 *
 * Description:
 *   This function will return the 256-bit message digest
 *   into the Message_Digest array provided by the caller.
 *   NOTE:
 *    The first octet of hash is stored in the element with index 0,
 *    the last octet of hash in the element with index 31.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to use to calculate the SHA hash.
 *   Message_Digest[ ]: [out]
 *     Where the digest is returned.
 *
 * Returns:
 *   sha Error Code.
 */
int SHA256Result(SHA256Context *context,
                 uint8_t Message_Digest[SHA256HashSize])
{
    return SHA224_256ResultN(context, Message_Digest, SHA256HashSize);
}

/*
 * SHA224_256Reset
 *
 * Description:
 *   This helper function will initialize the SHA256Context in
 *   preparation for computing a new SHA-224 or SHA-256 message digest.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to reset.
 *   H0[ ]: [in]
 *     The initial hash value array to use.
 *
 * Returns:
 *   sha Error Code.
 */
static int SHA224_256Reset(SHA256Context *context, uint32_t *H0)
{
    if (!context) return shaNull;

    context->length_high = context->length_low = 0;
    context->message_block_index  = 0;

    context->intermediate_hash[0] = H0[0];
    context->intermediate_hash[1] = H0[1];
    context->intermediate_hash[2] = H0[2];
    context->intermediate_hash[3] = H0[3];
    context->intermediate_hash[4] = H0[4];
    context->intermediate_hash[5] = H0[5];
    context->intermediate_hash[6] = H0[6];
    context->intermediate_hash[7] = H0[7];

    return shaSuccess;
}

/*
 * SHA224_256ProcessMessageBlock
 *
 * Description:
 *   This helper function will process the next 512 bits of the
 *   message stored in the message_block array.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *
 * Returns:
 *   Nothing.
 *
 * Comments:
 *   Many of the variable names in this code, especially the
 *   single character names, were used because those were the
 *   names used in the Secure Hash Standard.
 */
static void SHA224_256Transform(SHA256Context *context,
                                const uint8_t block[/*block_size*/])
{
    /* Constants defined in FIPS 180-3, section 4.2.2 */
    static const uint32_t K[64] = {
        0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b,
        0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
        0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7,
        0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
        0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152,
        0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
        0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
        0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
        0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819,
        0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
        0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f,
        0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
        0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };
    int        t, t4;                   /* Loop counter */
    uint32_t   temp1, temp2;            /* Temporary word value */
    uint32_t   W[64];                   /* Word sequence */
    uint32_t   A, B, C, D, E, F, G, H;  /* Word buffers */

    /*
     * Initialize the first 16 words in the array W
     */
    for (t = t4 = 0; t < 16; t++, t4 += 4)
        W[t] = (((uint32_t)block[t4]) << 24) |
        (((uint32_t)block[t4 + 1]) << 16) |
        (((uint32_t)block[t4 + 2]) << 8) |
        (((uint32_t)block[t4 + 3]));
    for (t = 16; t < 64; t++)
        W[t] = SHA256_sigma1(W[t-2]) + W[t-7] +
        SHA256_sigma0(W[t-15]) + W[t-16];

    A = context->intermediate_hash[0];
    B = context->intermediate_hash[1];
    C = context->intermediate_hash[2];
    D = context->intermediate_hash[3];
    E = context->intermediate_hash[4];
    F = context->intermediate_hash[5];
    G = context->intermediate_hash[6];
    H = context->intermediate_hash[7];

    for (t = 0; t < 64; t++) {
        temp1 = H + SHA256_SIGMA1(E) + SHA_Ch(E,F,G) + K[t] + W[t];
        temp2 = SHA256_SIGMA0(A) + SHA_Maj(A,B,C);
        H = G;
        G = F;
        F = E;
        E = D + temp1;
        D = C;
        C = B;
        B = A;
        A = temp1 + temp2;
    }

    context->intermediate_hash[0] += A;
    context->intermediate_hash[1] += B;
    context->intermediate_hash[2] += C;
    context->intermediate_hash[3] += D;
    context->intermediate_hash[4] += E;
    context->intermediate_hash[5] += F;
    context->intermediate_hash[6] += G;
    context->intermediate_hash[7] += H;

    context->message_block_index = 0;
}

/*
 * SHA224_256Finalize
 *
 * Description:
 *   This helper function finishes off the digest calculations.
 *
 * Parameters:
 *   context: [in/out]
 *     The SHA context to update.
 *   Pad_Byte: [in]
 *     The last byte to add to the message block before the 0-padding
 *     and length.  This will contain the last bits of the message
 *     followed by another single bit.  If the message was an
 *     exact multiple of 8-bits long, Pad_Byte will be 0x80.
 *
 * Returns:
 *   sha Error Code.
 */
static void SHA224_256Finalize(SHA256Context *context,
                               uint8_t Pad_Byte)
{
    int i;
    SHA224_256PadMessage(context, Pad_Byte);
    /* message may be sensitive, so clear it out */
    for (i = 0; i < SHA256_message_block_size; ++i)
    {
        context->message_block[i] = 0;
    }
    context->length_high = 0;     /* and clear length */
    context->length_low = 0;
}

/*
 * SHA224_256PadMessage
 *
 * Description:
 *   According to the standard, the message must be padded to the next
 *   even multiple of 512 bits.  The first padding bit must be a '1'.
 *   The last 64 bits represent the length of the original message.
 *   All bits in between should be 0.  This helper function will pad
 *   the message according to those rules by filling the
 *   message_block array accordingly.  When it returns, it can be
 *   assumed that the message digest has been computed.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to pad.
 *   Pad_Byte: [in]
 *     The last byte to add to the message block before the 0-padding
 *     and length.  This will contain the last bits of the message
 *     followed by another single bit.  If the message was an
 *     exact multiple of 8-bits long, Pad_Byte will be 0x80.
 *
 * Returns:
 *   Nothing.
 */
static void SHA224_256PadMessage(SHA256Context *context,
                                 uint8_t Pad_Byte)
{
    /*
     * Check to see if the current message block is too small to hold
     * the initial padding bits and length.  If so, we will pad the
     * block, process it, and then continue padding into a second
     * block.
     */
    if (context->message_block_index >= (SHA256_message_block_size-8)) 
    {
        context->message_block[context->message_block_index++] = Pad_Byte;
        while (context->message_block_index < SHA256_message_block_size)
        {
            context->message_block[context->message_block_index++] = 0;
        }
        SHA224_256Transform(context, context->message_block);
    }
    else
    {
        context->message_block[context->message_block_index++] = Pad_Byte;
    }

    while (context->message_block_index < (SHA256_message_block_size-8))
    {
        context->message_block[context->message_block_index++] = 0;
    }
    /*
     * Store the message length as the last 8 octets
     */
    context->message_block[56] = (uint8_t)(context->length_high >> 24);
    context->message_block[57] = (uint8_t)(context->length_high >> 16);
    context->message_block[58] = (uint8_t)(context->length_high >> 8);
    context->message_block[59] = (uint8_t)(context->length_high);
    context->message_block[60] = (uint8_t)(context->length_low >> 24);
    context->message_block[61] = (uint8_t)(context->length_low >> 16);
    context->message_block[62] = (uint8_t)(context->length_low >> 8);
    context->message_block[63] = (uint8_t)(context->length_low);

    SHA224_256Transform(context, context->message_block);
}

/*
 * SHA224_256ResultN
 *
 * Description:
 *   This helper function will return the 224-bit or 256-bit message
 *   digest into the Message_Digest array provided by the caller.
 *   NOTE:
 *    The first octet of hash is stored in the element with index 0,
 *    the last octet of hash in the element with index 27/31.
 *
 * Parameters:
 *   context: [in/out]
 *     The context to use to calculate the SHA hash.
 *   Message_Digest[ ]: [out]
 *     Where the digest is returned.
 *   HashSize: [in]
 *     The size of the hash, either 28 or 32.
 *
 * Returns:
 *   sha Error Code.
 */
static int SHA224_256ResultN(SHA256Context *context,
                             uint8_t Message_Digest[ ], int HashSize)
{
    int i;

    if (!context) return shaNull;
    if (!Message_Digest) return shaNull;

    SHA224_256Finalize(context, 0x80);

    for (i = 0; i < HashSize; ++i)
    {
        Message_Digest[i] = (uint8_t)  (context->intermediate_hash[i>>2] >> 8 *
                                        ( 3 - ( i & 0x03 ) ));
    }
    return shaSuccess;
}

