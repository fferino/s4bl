/*
   s4bl.c

 S4BL: a simple secure boot loader framework

 Copyright 2024 Fabrice Ferino

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#include "s4bl.h"

#include "s4bl_big_int.h"

/* if no macro created by the generator program are presented,
 * use global variables instead (testing)
 */
#ifndef N
extern const int N;
#endif

#ifndef PUB_EXP
extern const unsigned int PUB_EXP;
#endif

extern const S4BL_WORD *g_modulus; /* N + 1 */
extern const S4BL_WORD *g_mu; /* N + 1 */
extern S4BL_WORD *g_aux;     /* 6 * N + 2 */

#define SEQ         0x30
#define OID         0x06
#define DER_NULL    0x05, 0x00
#define OCTET_STR   0x04

#define SHA224_LEN      (28)
#define SHA256_LEN      (32)
#define SHA384_LEN      (48)
#define SHA512_LEN      (64)
#define SHA512_224_LEN  (28)
#define SHA512_256_LEN  (32)

#define ROOT_SHA2_OID   0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02

#define SHA2_DER(t,l)       { SEQ, l + 0x11,                \
                                SEQ, 0x0D,                  \
                                    OID, ROOT_SHA2_OID, t,  \
                                    DER_NULL,               \
                                OCTET_STR, l }

#define DER_PREFIX_LEN (19)

typedef const unsigned char der_prefix_t[DER_PREFIX_LEN];


/* place SHA2_DER at id - 1 to start at array index 0 */
#define DECL_SHA2_DER(a)  [S4BL_##a - 1] = SHA2_DER(S4BL_##a, a##_LEN)

/* ISO C90 forbids specifying subobject to initialize
 * But the order of these in the array is very important
 * so disable this error as recommended by GCC itself
 */
#ifdef __STRICT_ANSI__
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpedantic"
#endif

static const der_prefix_t k_der_prefixes[S4BL_DIGEST_ALGO_LAST] =
{
    /* order of this so that k_der_prefixes[i][14] == [i+1] */
    DECL_SHA2_DER(SHA256),
    DECL_SHA2_DER(SHA384),
    DECL_SHA2_DER(SHA512),
    DECL_SHA2_DER(SHA224),
    DECL_SHA2_DER(SHA512_224),
    DECL_SHA2_DER(SHA512_256),
};

#ifdef __STRICT_ANSI__
#pragma GCC diagnostic pop
#endif

const unsigned char *S4BL_pkcs1v15_signature_data(const S4BL_WORD *signature,
                                                  int *data_len)
{
    int i;
    unsigned char *ret = 0;
    unsigned char *t;
    const S4BL_WORD *curr;
    int shift;
    /* reorganize the result (S4BL_WORD*) in the beginning of aux into the
     * work area as unsigned char
     */

    const S4BL_WORD *result = S4BL_barrett_modexp(N, signature, PUB_EXP,
                                                  g_modulus, g_mu, g_aux);

    if (!result)
    {
        return 0;
    }
    /* most significant 2 bytes should be 0x00, 0x01 */
    /* EB = 00 || BT || PS || 00 || D */
    curr = result + N - 1;
    shift = S4BL_WORD_NBITS - (2 * __CHAR_BIT__);
    if ( (((*curr) >> shift) & 0xFFFF) != 0x0001)
    {
        return 0;
    }
    shift -= __CHAR_BIT__;

    for ( i = N - 1; i >= 0; --i, shift = S4BL_WORD_NBITS - __CHAR_BIT__)
    {
        curr = result + i;
        for (; shift >= 0; shift -= __CHAR_BIT__)
        {
            if ( (((*curr) >> shift) & 0xFF) != 0xFF)
            {
                goto padding_done;
            }
        }
    }

padding_done:
    /* defensively test for i == 0 even though *current* subsequent code
     * is able to to deal with that
     */
    /* current one (after 0xFF s) should be 00 */
    if ( i == 0 || (((*curr) >> shift) & 0xFF) != 0x00)
    {
        return 0;
    }

    /* consume the 0 byte */
    if (shift == 0)
    {
        shift = S4BL_WORD_NBITS;
        --i;
    }
    shift -= __CHAR_BIT__;

    /* work area */

    t = ret = (unsigned char *)(g_aux + 4 * N);
    for ( ; i >= 0; --i, shift = S4BL_WORD_NBITS - __CHAR_BIT__)
    {
        curr = result + i;
        for (; shift >= 0; shift -= __CHAR_BIT__)
        {
            *t++ = (((*curr) >> shift) & 0xFF);
        }
    }
    *data_len = (int) (t - ret);
    return ret;
}


S4BL_BOOL S4BL_verify_signature(S4BL_digest_algo_t algo,
                                const unsigned char *digest,
                                const S4BL_WORD *signature /* N */)
{
    int i, cmp_res, unpadded_len, expected_len;
    const volatile unsigned char *unpadded;
    const unsigned char *expected_prefix;
    /* sanity checks */
    if (algo >= S4BL_DIGEST_ALGO_LAST)
    {
        return S4BL_FALSE;
    }

    unpadded = S4BL_pkcs1v15_signature_data(signature, &unpadded_len);
    if (unpadded == 0)
    {
        /* no PKCS1v15 recovered from verification */
        return S4BL_FALSE;
    }
    /* unpadded len is the DER encoded ASN.1 structure
     SEQUENCE (2 elem)
        SEQUENCE (2 elem)
            OBJECT IDENTIFIER
            NULL
        OCTET STRING digest
     */
    /* retrieve ASN.1 prefix for this algo */
    expected_prefix = k_der_prefixes[algo - 1];
    /* length of digest is last byte of der prefix */
    expected_len = expected_prefix[DER_PREFIX_LEN - 1];
    if (expected_len  + DER_PREFIX_LEN != unpadded_len)
    {
        return S4BL_FALSE;
    }

    cmp_res = 0;
    for ( i = 0; i < DER_PREFIX_LEN; ++i)
    {
        cmp_res |= (unpadded[i] ^ expected_prefix[i]);
    }
    for (; i < unpadded_len; ++i, ++digest)
    {
        cmp_res |= (unpadded[i] ^ *digest);
    }
    return (cmp_res == 0)? S4BL_TRUE : S4BL_FALSE;
}
