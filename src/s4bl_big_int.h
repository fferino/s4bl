/*
  s4bl_big_int.h

  Arbitrary Precision Integer for S4BL

 Copyright 2024 Fabrice Ferino

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#ifndef s4bl_big_int_h
#define s4bl_big_int_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

#if __CHAR_BIT__ != 8
#error Unsupported platform
#endif

#ifndef S4BL_WORD_SIZE
#define S4BL_WORD_SIZE  16
#endif

#if S4BL_WORD_SIZE == 16
typedef __uint128_t S4BL_WORD;
#elif S4BL_WORD_SIZE == 8
typedef uint64_t S4BL_WORD;
#elif S4BL_WORD_SIZE == 4
typedef uint32_t S4BL_WORD;
#elif S4BL_WORD_SIZE == 2
typedef uint16_t S4BL_WORD;
#else
#pragma error unsupported S4BL_WORD_SIZE
#endif

#define S4BL_WORD_NBITS   ((int) (S4BL_WORD_SIZE * __CHAR_BIT__))

unsigned int S4BL_sub(int n, S4BL_WORD *a, const S4BL_WORD *b);
int S4BL_cmp(int n, const S4BL_WORD *a, const S4BL_WORD* b);
void S4BL_mul(int n, S4BL_WORD* c, const S4BL_WORD* a, const S4BL_WORD *b);
const S4BL_WORD *S4BL_barrett_modexp(int num_words, const S4BL_WORD *x,
                                     unsigned int e, const S4BL_WORD *modulus,
                                     const S4BL_WORD *mu, S4BL_WORD *aux);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* s4bl_big_int_h */
