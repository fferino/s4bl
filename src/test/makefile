#
# makefile for testing
#
#  s4bl: simple secure boot loader
#
#  Copyright 2024 Fabrice Ferino
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

UNAME:=$(shell uname)

UNAME_M:=$(shell uname -m)

TARGET?=$(UNAME_M)

# prefer static builds when possible (QEMU on Linux)
ifeq ($(UNAME), Linux)
EXTRA_FLAGS = -static -ffat-lto-objects
endif

ifneq ($(TARGET),)
include ../arch/$(TARGET).mk
endif

# cross compilation
ifdef CROSS_COMPILE
CC=$(CROSS_COMPILE)-gcc
CXX=$(CROSS_COMPILE)-g++
BUILD_SUBDIR=$(TARGET)
else
BUILD_SUBDIR=native
endif

# directories
SRCS_DIR:=..
BUILD_DIR:=build/$(BUILD_SUBDIR)


CPPFLAGS+=-I $(SRCS_DIR)

CFLAGS=-Wall -Werror -Wextra -Wpedantic -Wshadow -Wconversion -g -std=c89 \
	-Os -flto -ffunction-sections -fdata-sections $(EXTRA_FLAGS)

CXXFLAGS=-Wall -Werror -Wextra -Wpedantic -Wshadow -Wconversion -g -std=c++2b \
	-Os -flto $(EXTRA_FLAGS)

LDFLAGS=-flto -g $(EXTRA_FLAGS)

LDLIBS=-lstdc++ -lm

OBJS=test_driver.o s4bl_big_int_hex.o s4bl_sha_test.o \
	s4bl_big_int.o s4bl.o \
	sha224-256.o sha384-512.o

BUILD_OBJS=$(addprefix $(BUILD_DIR)/, $(OBJS))
BUILD_TEST=$(BUILD_DIR)/test_driver


all: $(BUILD_TEST)

#### building

$(BUILD_TEST): $(BUILD_OBJS)

$(BUILD_DIR)/%.o: $(SRCS_DIR)/%.c | $(BUILD_DIR)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

$(BUILD_DIR)/%.o: %.c | $(BUILD_DIR)
	$(COMPILE.c) $(OUTPUT_OPTION) $<

$(BUILD_DIR)/%.o: %.cpp | $(BUILD_DIR)
	$(COMPILE.cc) $(OUTPUT_OPTION) $<

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)


### Testing

test: key_test sha_test speed_test

key_test: $(BUILD_TEST)
	$(EXEC_PREFIX) $(BUILD_TEST) ./data/*.txt

speed_test: speed-2048 speed-4096

speed-%: $(BUILD_TEST)
	$(EXEC_PREFIX) $(BUILD_TEST) speed ./data/verify$*-200.txt

# test all supported algorithms
sha_test: sha224_test sha256_test sha384_test sha512_test


# 2 types of test files

sha%_test: SHA%Short.bin SHA%Long.bin
	$(EXEC_PREFIX) $(BUILD_TEST) SHA$* $^

SHA%.bin:
	python3 ../../tool/cavp_test.py < ./data/shabytetestvectors/SHA$*Msg.rsp > $@

clean:
	rm -rf $(BUILD_DIR)

# make .FOO -> prints $FOO
.% : ;@echo  '$* = $($*)'

.NOTPARALLEL: speed_test

.PHONY: clean test
