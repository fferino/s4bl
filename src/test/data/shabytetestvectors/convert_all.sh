#!/usr/bin/env bash

for f in *.rsp; do
	../../../../tool/cavp_test.py < ${f} > ${f//.rsp/.bin}
done
