/*
  s4bl_sha_test.h

  SHA2 testing

 Copyright 2024 Fabrice Ferino
 SPDX-License-Identifier: Apache-2.0

*/

#ifndef s4bl_sha_test_h
#define s4bl_sha_test_h

#include "s4bl.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int (*sha_reset_func)(void *ctx);
typedef int (*sha_update_func)(void *ctx,
                               const uint8_t *bytes,
                               unsigned int bytecount);
typedef int (*sha_final_func)(void *ctx, uint8_t *digest);


typedef struct
{
    S4BL_digest_algo_t      algo_id;
    unsigned int            digest_size;
    sha_reset_func          reset_f;
    sha_update_func         update_f;
    sha_final_func          final_f;
} sha2_algo_class_t;

int sha_test(sha2_algo_class_t *algo, void *algo_ctx,
             int argc, const char *argv[]);

#ifdef __cplusplus
} // extern "C"
#endif


#endif /* s4bl_sha_test_h */
