//
//  test_driver.cpp
//  s4bl
//
//  s4bl: simple secure boot loader
//
//  Copyright 2024 Fabrice Ferino
//  SPDX-License-Identifier: Apache-2.0
//

#include <string>
#include <vector>
#include <charconv>
#include <fstream>
#include <exception>
#include <memory>
#include <algorithm>
#include <unordered_map>

#include <chrono>
using namespace std::chrono;

#include <cstring>
#include <cassert>

#include "s4bl.h"
#include "sha.h"
#include "s4bl_big_int_hex.h"
#include "s4bl_sha_test.h"

/* external entities */
extern "C"
{
int N;
unsigned int PUB_EXP;
const S4BL_WORD *g_modulus;
const S4BL_WORD *g_mu;
S4BL_WORD *g_aux;
}

#define NUM_OPS_SPEED (10000)

#define ARRAY_COUNT(a)   (sizeof(a)/sizeof((a)[0]))

/* hash algos */
typedef struct
{
    void *ctx;
    sha2_algo_class_t algo_class;
} sha2_algo_exec_t;

#define DEFINE_CTX(a) static a##Context m_##a##_ctx

#define ALGO_CLASS_ENTRY(a)						            \
{ #a , { (void *) &m_##a##_ctx,                             \
{ S4BL_##a, a##HashSize, (sha_reset_func) a##Reset,         \
(sha_update_func) a##Input, (sha_final_func)a##Result }} }

#define UNIMPL_ALGO_CLASS_ENTRY(a)                          \
{ #a , { NULL,                                              \
{ S4BL_##a, UINT_MAX, (sha_reset_func) NULL,                \
(sha_update_func) NULL, (sha_final_func)NULL }} }


DEFINE_CTX(SHA224);
DEFINE_CTX(SHA256);
DEFINE_CTX(SHA384);
DEFINE_CTX(SHA512);


std::unordered_map<std::string, sha2_algo_exec_t> kAlgoString2Class =
{
    ALGO_CLASS_ENTRY(SHA224),
    ALGO_CLASS_ENTRY(SHA256),
    ALGO_CLASS_ENTRY(SHA384),
    ALGO_CLASS_ENTRY(SHA512),
    UNIMPL_ALGO_CLASS_ENTRY(SHA512_224),
    UNIMPL_ALGO_CLASS_ENTRY(SHA512_256),
};

typedef std::vector<uint8_t> byte_array;

typedef std::unique_ptr<char, decltype([](void *p) { std::free(p); })>
C_autofree;

void read_n_lines(std::ifstream &ifs, int n, std::string sv[/*n*/])
{
    for (int i = 0; i < n; ++i)
    {
        if (!std::getline(ifs, sv[i]))
        {
            printf("Invalid file format\n");
            throw i;
        }
    }
}

byte_array hex2bytes(const std::string& s)
{
    byte_array v;
    size_t ssize = s.size();
    v.reserve((ssize + 1) / 2);
    const char *ss = s.c_str();
    uint8_t val;
    size_t i = 0;

    if (ssize & 1)
    {
        std::from_chars(ss, ss+1, val, 16);
        v.push_back(val);
        i = 1;
    }

    for (; i < ssize; i += 2)
    {
        std::from_chars(ss+i, ss+i+2, val, 16);
        v.push_back(val);
    }
    return v;
}

size_t hexstring_bitlength(const std::string &s)
{
    /* key_bits: each hex digit is 4 bits - number of zero bits in first byte */
    for (size_t i = 0; i < s.size(); ++i)
    {
        if (s[i] != '0')
        {
            uint8_t val;
            std::from_chars(s.c_str()+i, s.c_str()+i+1, val, 16);
            size_t clear_bits = static_cast<size_t>(__builtin_clz(val));
            size_t set_bits = (sizeof(int) * 8) - clear_bits;
            return 4 * (s.size() - i - 1) + set_bits;
        }
    }
    return 0;
}



int S4BL_compare_hex_repr(const char *repr, const char *model)
{
    /* model (externally generated) can have several leading zeroes */
    while (*model == '0')
    {
        ++model;
    }
    /* repr (externally generated) can have at most one leading zero
     * which is removed in case the model had an odd number of hex nibbles
     */

    return strcasecmp(model, repr + ((*repr == '0') ? 1 : 0));
}

class CS4BL
{
public:
    CS4BL(): _w{nullptr}, _n{0} {}

    CS4BL(const char *s)
    {
        _n = S4BL_hex2word(s, &_w);
    }

    CS4BL( const std::string & s)
    {
        _n = S4BL_hex2word(s.c_str(), &_w);
    }

    ~CS4BL()
    {
        free(_w);
        _w = nullptr;
        _n = 0;
    }

    void set_bigger_n(size_t new_n)
    {
        if (new_n < _n)
        {
            throw std::runtime_error("cannot resize S4BL to smaller size");
        }

        S4BL_WORD* new_w = (S4BL_WORD *) realloc( _w, new_n * S4BL_WORD_SIZE);
        if (!new_w)
        {
            throw std::bad_alloc();
        }
        // set new S4BL_WORDS to zero
        memset(new_w + _n, 0, (new_n - _n) * S4BL_WORD_SIZE);
        _w = new_w;
        _n = new_n;
    }

    size_t size()
    {
        return _n;
    }

    C_autofree repr() const
    {
        return C_autofree{S4BL_word2hex(_w, static_cast<int>(_n)) };
    }


    operator S4BL_WORD*()
    {
        return _w;
    }

    operator const S4BL_WORD*() const
    {
        return _w;
    }

private:

    S4BL_WORD *_w;
    size_t   _n;
};

int ExecuteS4BLBinaryTest(const std::string &op, const std::string *ops,
                          bool /* speed */)
{
    int errs = 0;

    /* create S4BL from lines */
    CS4BL first {ops[0]}, second {ops[1]};

    /* make them all the same size plus one (carry) */
    size_t max_size = std::max( first.size(), second.size()) + 1;

    /* adjust sizes */
    first.set_bigger_n(max_size);
    second.set_bigger_n(max_size);

    unsigned int r = 0;
    if (op == "-")
    {
        r = S4BL_sub(static_cast<int>(max_size), first, second);
    }
    else
    {
        printf("Invalid binary op %s\n", op.c_str());
        return 1;
    }

    C_autofree result = first.repr();
    if (r)
    {
        errs += 1;
        printf("%s should be zero!\n",
               op == "+" ? "carry" : "borrow");
    }
    if ( S4BL_compare_hex_repr(result.get(), ops[2].c_str()) )
    {
        errs += 1;
        printf("error for %s:\nexp: %s\ngot: %s\n\n",
               op.c_str(), ops[2].c_str(), result.get());
    }

    return errs;
}

int ExecuteS4BLCmpTest(const std::string *ops, int res, bool /* speed */)
{
    int errs = 0;

    /* create S4BL from lines */
    CS4BL first {ops[0]}, second {ops[1]};

    /* make them all the same size */
    size_t max_size = std::max( first.size(), second.size());

    /* adjust sizes */
    first.set_bigger_n(max_size);
    second.set_bigger_n(max_size);

    int r = S4BL_cmp(static_cast<int>(max_size), first, second);
    if (r != res)
    {
        errs += 1;
        printf("error:\nop1: %s\nop2: %s\nexpected: %d got %d\n\n",
               ops[0].c_str(), ops[1].c_str(), res, r);
    }
    return errs;
}

int ExecuteS4BLMulTest(const std::string *ops, bool /* speed */)
{
    int errs = 0;

    /* create S4BL from lines */
    CS4BL first {ops[0]}, second {ops[1]}, product;

    /* make both operands the same size */
    size_t max_size = std::max( first.size(), second.size());

    /* adjust sizes */
    first.set_bigger_n(max_size);
    second.set_bigger_n(max_size);

    /* make product the correct size */
    product.set_bigger_n(2 * max_size);

    S4BL_mul(static_cast<int>(max_size), product, first, second);

    C_autofree result = product.repr();
    if ( S4BL_compare_hex_repr(result.get(), ops[2].c_str()) )
    {
        errs += 1;
        printf("error for *:\nexp: %s\ngot: %s\n\n",
               ops[2].c_str(), result.get());
    }
    return errs;
}


extern "C"
const unsigned char *S4BL_pkcs1v15_signature_data(const S4BL_WORD *signature,
                                                  int *data_len);

int ExecuteS4BLModExpTest(const std::string *ops, bool speed)
{
    int errs = 0;

    /* create S4BL from lines x, modulus, mu */
    CS4BL x {ops[0]}, modulus {ops[2]}, mu {ops[3]};
    auto pub_exp = static_cast<unsigned int>(std::stoul(ops[1], nullptr, 16));

    /* size sanity */
    size_t n = std::max({x.size(), modulus.size(), mu.size()-1});

    x.set_bigger_n(n);
    modulus.set_bigger_n(n+1);
    mu.set_bigger_n(n+1);

    /* Auxiliary buffer */
    CS4BL aux;
    aux.set_bigger_n(6 * n + 2);

    if (speed)
    {
        auto start = high_resolution_clock::now();
        for (int i = 0; i < NUM_OPS_SPEED; ++i)
        {
            S4BL_barrett_modexp(static_cast<int>(n), x,
                                pub_exp,
                                modulus, mu, aux);
        }
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        printf("%d barrett modexp in %lld milliseconds\n",
               NUM_OPS_SPEED, (unsigned long long) duration.count());
        double elapsed_seconds = ((double) duration.count()) / 1000;
        printf("barrett modexp: %g seconds\n", elapsed_seconds / NUM_OPS_SPEED);
        printf("barrett modexp: %g/second\n", NUM_OPS_SPEED / elapsed_seconds);

        return 0;
    }

    N = static_cast<int>(n);
    PUB_EXP = pub_exp;
    g_modulus = modulus;
    g_mu = mu;
    g_aux = aux;

    const unsigned char *signature_data;
    int signature_data_len;
    signature_data = S4BL_pkcs1v15_signature_data(x, &signature_data_len);

    if (signature_data)
    {
        if (signature_data_len * 2 != (int) (ops[4].size()))
        {
            printf("Signature data expected lengh %d; got %d\n",
                   (int)(ops[4].size())/2, signature_data_len);
            ++errs;
        }
        // verify match
        byte_array expected_signature = hex2bytes(ops[4]);
        errs += std::memcmp(expected_signature.data(),
                            signature_data,
                            expected_signature.size()) ? 1 : 0;
    }
    else
    {
        //fprintf(stderr, "Unknow digest algorithm %s\n", ops[4].c_str());
        ++errs;
    }
    return errs;
}


int ExecuteS4BLVerifyTest(const std::string *ops, bool speed)
{
    int errs = 0;

    /* create S4BL from lines x, modulus, mu */
    CS4BL x {ops[0]}, modulus {ops[2]}, mu {ops[3]};

    auto pub_exp = static_cast<unsigned int>(std::stoul(ops[1], nullptr, 16));

    /* size sanity */

    size_t n = std::max({x.size(), modulus.size(), mu.size()-1});

    x.set_bigger_n(n);
    modulus.set_bigger_n(n+1);
    mu.set_bigger_n(n+1);

    /* Auxiliary buffer */
    CS4BL aux;
    aux.set_bigger_n(6 * n + 2);

    auto f = kAlgoString2Class.find(ops[4]);
    if (f != kAlgoString2Class.end())
    {
        N = static_cast<int>(n);
        PUB_EXP = pub_exp;
        g_modulus = modulus;
        g_mu = mu;
        g_aux = aux;

        byte_array digest = hex2bytes(ops[5]);
        if (speed)
        {
            auto start = high_resolution_clock::now();
            for (int i = 0; i < NUM_OPS_SPEED; ++i)
            {
                S4BL_verify_signature((*f).second.algo_class.algo_id,
                                      digest.data(), x);
            }
            auto stop = high_resolution_clock::now();
            auto duration = duration_cast<milliseconds>(stop - start);
            printf("%d verifications in %lld milliseconds\n",
                   NUM_OPS_SPEED, (unsigned long long) duration.count());
            double elapsed_seconds = ((double) duration.count()) / 1000;
            printf("verifications: %g seconds\n", elapsed_seconds / NUM_OPS_SPEED);
            printf("verifications: %g/second\n", NUM_OPS_SPEED / elapsed_seconds);

            return 0;
        }

        bool verify = S4BL_verify_signature((*f).second.algo_class.algo_id,
                                            digest.data(), x);

        if (!verify)
        {
            ++errs;
            /* TO DO: print recovered value ?*/
        }
    }
    else
    {
        printf("Unknow digest algorithm %s\n", ops[4].c_str());
        ++errs;
    }
    return errs;
}


int process_file(const char *fname, bool speed)
{
    int errs = 0;
    int tests = 0;
    try
    {
        fprintf(stderr, "%s :", fname);
        std::ifstream ifs(fname);
        std::string line;
        std::string op_lines[6];
        while (std::getline(ifs, line))
        {
            if (line == "-")
            {
                read_n_lines(ifs, 3, op_lines);
                errs += ExecuteS4BLBinaryTest(line, op_lines, speed);
            }
            else if (line == "*")
            {
                read_n_lines(ifs, 3, op_lines);
                errs += ExecuteS4BLMulTest(op_lines, speed);
            }
            else if (line == "cmp" )
            {
                read_n_lines(ifs, 3, op_lines);
                errs += ExecuteS4BLCmpTest(op_lines,
                                           atoi(op_lines[2].c_str()),
                                           speed);
            }
            else if (line == "modexp")
            {
                /* x, e, mod, mu, decrypted */
                read_n_lines(ifs, 5, op_lines);
                errs += ExecuteS4BLModExpTest(op_lines, speed);
            }
            else if (line == "verify")
            {
                /* x, e, mod, mu, hash_algo, digest */
                read_n_lines(ifs, 6, op_lines);
                errs += ExecuteS4BLVerifyTest(op_lines, speed);
            }
            else if (line.starts_with("modexp"))
            {
                /* negative test */
                read_n_lines(ifs, 5, op_lines);

                if  (ExecuteS4BLModExpTest(op_lines, speed) == 0)
                {
                    errs += 1;
                }
            }
            else if (line.starts_with("verify"))
            {
                /* negative test */
                read_n_lines(ifs, 6, op_lines);
                if (ExecuteS4BLVerifyTest(op_lines, speed) == 0)
                {
                    errs += 1;
                }
            }
            else
            {
                continue;
            }

            tests+=1;
            if (speed)
            {
                return errs;
            }
        }
    }
    catch (...)
    {
        errs += 1;
    }
    fprintf(stderr, "%d errs in %d tests\n", errs, tests);
    return errs;
}



int file_based_test(int argc, const char *argv[], bool speed)
{
    int errs = 0;
    for (int i = 0; i < argc; ++i)
    {
        errs += process_file(argv[i], speed);
    }
    return errs;
}

struct test_case
{
    const char *in;
}
io_tests[] =
{
    {""},
    {"1234"},
    {"1234567"},
    {"ABCDEF0123456789"},
    {"0123456789ABCDEF"},
    {"123456789ABCDEF"},
    {"123456789ABCDEF01"},
    {"123456789ABCDEF012"},
    {"123456789ABCDEF0123"},
    {"123456789ABCDEF01234"},
    {"123456789ABCDEF012345"},
    {"123456789ABCDEF0123456"},
    {"123456789ABCDEF01234567"},
    {"123456789ABCDEF012345678"},
    {"123456789ABCDEF0123456789A"},
    {"123456789ABCDEF0123456789AB"},
    {"123456789ABCDEF0123456789Abc"},
    {"123456789ABCDEF0123456789Abcd"},
    {"123456789ABCDEF0123456789Abcde"},
    {"123456789ABCDEF0123456789AbcdeF"},
    {"123456789ABCDEF0"},
    {"1234567890abcdef1234567"},
};

int Test_S4BL_hex(void)
{
    int errs = 0;
    for (size_t i = 0; i < ARRAY_COUNT(io_tests); ++i)
    {
        CS4BL S4BL(io_tests[i].in);
        C_autofree ss = S4BL.repr();

        /* compare both: should be identical, case insensitive after
         * trimming leading zeroes */

        if ( S4BL_compare_hex_repr(ss.get(), io_tests[i].in) )
        {
            errs += 1;
            printf("error:\nin:  %s\nout: %s\n\n",
                   io_tests[i].in, ss.get());
        }
    }
    return errs;
}

bool gTestCompleted;

void exit_func(void)
{
    if (!gTestCompleted)
    {
        printf("Abort called: internal logic error!\n");
    }
}

int main(int argc, const char * argv[])
{
    int errs = 0;

    atexit(exit_func);
    gTestCompleted = false;
    if (argc < 2)
    {
        errs += Test_S4BL_hex();
    }
    else if (strcmp(argv[1], "speed") == 0)
    {
        errs += file_based_test(argc-2, argv+2, true);
    }
    else if (strncmp(argv[1], "SHA", 3) == 0)
    {
        auto found = kAlgoString2Class.find(argv[1]);
        if (found != kAlgoString2Class.end())
        {
            errs += sha_test(&(*found).second.algo_class,
                             (*found).second.ctx,
                             argc-2,
                             argv+2);
        }
        else
        {
            printf("Unknown sha algorithm \"%s\"\n", argv[1]);
            errs += 1;
        }
    }
    else
    {
        errs += file_based_test(argc-1, argv+1, false);
    }
    printf("%d errors\n", errs);
    gTestCompleted = true;
    return errs;
}
