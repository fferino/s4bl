/*
  s4bl_big_int_hex.h

  Conversion hex strings <=> S4BL_WORD

 Copyright 2024 Fabrice Ferino
 SPDX-License-Identifier: Apache-2.0

*/

#ifndef s4bl_big_int_hex_h
#define s4bl_big_int_hex_h

#ifdef __cplusplus
extern "C" {
#endif

#include "s4bl_big_int.h"
#include <stddef.h>

char* S4BL_word2hex(const S4BL_WORD *m, int n);
size_t S4BL_hex2word(const char *s, S4BL_WORD **m);

void S4BL_print(const char *msg, const S4BL_WORD *w, int n);

#ifdef __cplusplus
} // extern "C"
#endif


#endif /* s4bl_big_int_hex_h */
