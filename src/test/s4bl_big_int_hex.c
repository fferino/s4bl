/*
  s4bl_big_int_hex.c

  Conversion hex strings <=> S4BL_WORD

 Copyright 2024 Fabrice Ferino
 SPDX-License-Identifier: Apache-2.0

*/


#include "s4bl_big_int_hex.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

static const char nibble_char[] = "0123456789ABCDEF";

static long S4BL_word2hex_aux(S4BL_WORD w, char *s, int *trim_zeroes)
{
    int i = S4BL_WORD_SIZE - 1;
    char *t = s;
    if (*trim_zeroes)
    {
        for (; i >= 0; --i)
        {
            uint8_t b = (w >> (i * 8)) & 0xFF;
            if (b)
            {
                *trim_zeroes = 0;
                break;
            }
        }
    }

    for (; i >= 0; --i)
    {
        uint8_t b = (w >> (i * 8)) & 0xFF;
        *t++ = nibble_char[b >> 4];
        *t++ = nibble_char[b & 0xF];
    }
    return t - s;
}

char* S4BL_word2hex(const S4BL_WORD *m, int n)
{
    size_t alloc_size = ((size_t) n) * S4BL_WORD_SIZE * 2 + 1;
    char* ret = (char *) malloc(alloc_size);
    long char_count = 0;
    int i, trimzeroes = 1;
    for (i = n-1; i >= 0; --i)
    {
        char_count += S4BL_word2hex_aux(m[i], ret + char_count, &trimzeroes);
    }
    ret[char_count] = '\0';
    return ret;
}

static int hexval(char c)
{
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    if (c >= 'a' && c <= 'f')
    {
        return c - 'a' + 10;
    }
    if (c >= 'A' && c <= 'F')
    {
        return c - 'A' + 10;
    }
    assert(0);
}


static S4BL_WORD S4BL_hex2word_aux(const char *t)
{
    int i;
    S4BL_WORD val = 0;
    for (i = 0; i < S4BL_WORD_SIZE * 2 && *t; ++i, ++t)
    {
        val *= 16;
/* spurious GCC error: conversion to ‘S4BL_WORD’ {aka ‘__int128 unsigned’}
 * from ‘__int128 unsigned’ may change the sign of the result
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
        val += (S4BL_WORD) hexval(*t);
#pragma GCC diagnostic pop
    }
    return val;
}


size_t S4BL_hex2word(const char *s, S4BL_WORD **m)
{
    size_t byte_len, word_len;
    size_t s_len = strlen(s);
    const char *t;
    S4BL_WORD *w;

    if (s_len == 0)
    {
        *m = (S4BL_WORD *) malloc(S4BL_WORD_SIZE);
        (*m)[0] = 0;
        return 1;
    }
    byte_len = (s_len + 1)/2;
    word_len = (byte_len + S4BL_WORD_SIZE - 1) / S4BL_WORD_SIZE;

    *m = (S4BL_WORD *) malloc(word_len * S4BL_WORD_SIZE);

    t = s + s_len;
    w = *m;

    while (t >= s + S4BL_WORD_SIZE * 2)
    {
        t -= S4BL_WORD_SIZE * 2;
        *w++ = S4BL_hex2word_aux(t);
    }
    if (s != t)
    {
        int i;
        *w = 0;
        for (i = 0; s + i != t; ++i)
        {
            *w *= 16;
/* spurious GCC error: conversion to ‘S4BL_WORD’ {aka ‘__int128 unsigned’}
 * from ‘__int128 unsigned’ may change the sign of the result
 */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
            *w += (S4BL_WORD) hexval(s[i]);
#pragma GCC diagnostic pop

        }
    }
    return word_len;
}

void S4BL_print(const char *msg, const S4BL_WORD *w, int n)
{
    char *repr = S4BL_word2hex(w, n);
    printf("%s [%d]: %s\n", msg, n, repr);
    free(repr);
}
