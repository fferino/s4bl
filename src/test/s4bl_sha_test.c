/*
  s4bl_sha_test.c

  SHA2 testing

 Copyright 2024 Fabrice Ferino
 SPDX-License-Identifier: Apache-2.0

*/

#include "s4bl_sha_test.h"
#include <stdio.h>
#include "sha.h"


typedef size_t (*read_func)(void *arg, void *buffer, size_t num_bytes);

size_t read_from_CFile(void *arg, void *buffer, size_t num_bytes)
{
    return fread(buffer, 1, num_bytes, (FILE*) arg);
}

void print_buffer(const char *msg, uint8_t *buff, int size)
{
    int i;
    fprintf(stderr, "%s:\n", msg);
    for (i = 0; i < size; ++i)
    {
        fprintf(stderr, "%02x:", buff[i]);
        if ((i & 0x0F) == 0x0F)
        {
            fprintf(stderr, "\n");
        }
    }
    fprintf(stderr, "\n");
}

static int sha_stream_test(sha2_algo_class_t *algo,
                           void *algo_ctx,
                           void *stream,
                           read_func readf)
{
    /*
      read from an NIST SHA test file (non "montecarlo")
      stream of bytes:
      [message_length: 2 bytes big endian,
      message: message_length bytes,
      expected_digest: digest_length bytes]
    */

    unsigned int i;
    int mismatch, errs = 0, test = 0;

    for (;;)
    {
        /* read length 2 BE bytes */
        uint16_t msg_len;

        uint8_t buff[SHA512HashSize];

        if (readf(stream, &buff, sizeof(msg_len)) != sizeof(msg_len))
        {
            break;
        }
        msg_len = (uint16_t) (buff[0] << 8) + buff[1];

        /*
         msg_len = be16toh(msg_len);
         */
        algo->reset_f(algo_ctx);

        for (i = 0; i + SHA512HashSize < msg_len; i += SHA512HashSize)
        {
            if (readf(stream, buff, SHA512HashSize) != SHA512HashSize)
            {
                printf("test %d: unable to read complete message: "
                       "read = %d msg length = %d\n",
                       test, i, msg_len);
                errs += 1;
                goto exit;
            }
            algo->update_f(algo_ctx, buff, SHA512HashSize);
        }

        if (i < msg_len)
        {
            unsigned remain = msg_len - i;
            if (readf(stream, buff, remain) != remain)
            {
                printf("test %d: unable to read complete message: "
                       "read = %d msg length = %d\n",
                       test, i, msg_len);

                errs += 1;
                goto exit;
            }
            algo->update_f(algo_ctx, buff, remain);
        }

        algo->final_f(algo_ctx, buff);

        mismatch = 0;
        /* read expected value and compare */
        for (i = 0; i < algo->digest_size; i += sizeof(msg_len))
        {
            if (readf(stream, &msg_len, sizeof(msg_len)) != sizeof(msg_len))
            {
                printf("test %d: error reading expected digest\n",
                       test);
                errs += 1;
                goto exit;
            }

            if (msg_len != * ((uint16_t *) (buff + i)) )
            {
                mismatch = 1;
            }
        }
        errs += mismatch;
        ++test;
    }
    printf("%d errs in %d tests\n", errs, test);
exit:
    return errs;

}

static int sha_file_test(sha2_algo_class_t *algo,
                         void *algo_ctx,
                         const char *fname)
{
    int errs = 0;
    uint8_t buffer[2];
    FILE *f = fopen(fname, "r");
    if (!f)
    {
        fprintf(stderr, "Unable to open SHA test file %s\n", fname);
        return 1;
    }
    /* read the first 2 types: length of digest */
    if (fread(&buffer, 1, sizeof(buffer), f) != sizeof(buffer))
    {
        fprintf(stderr, "Unable to read digest length from SHA test file %s\n",
                fname);
        errs+= 1;
        goto exit;
    }
    if (buffer[0] != 0 || buffer[1] != algo->digest_size)
    {
        fprintf(stderr, "Mismatch digest size: expected %d read %d\n",
                algo->digest_size, (buffer[0] << 16) + buffer[1]);
        errs+= 1;
        goto exit;
    }

    errs = sha_stream_test(algo, algo_ctx, f, read_from_CFile);

exit:
    fclose(f);
    return errs;
}


int sha_test(sha2_algo_class_t *algo, void *algo_ctx,
             int argc, const char *argv[])
{
    int i, errs = 0;

    for (i = 0; i < argc; ++i)
    {
       errs += sha_file_test(algo, algo_ctx, argv[i]);
    }
    return errs;
}
