/*
   s4bl_big_int.c

  Arbitrary Precision Integer for S4BL

 Copyright 2024 Fabrice Ferino

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

#include "s4bl_big_int.h"

#define XSTR(x) STR(x)
#define STR(x) #x

#define MIN(a,b) ((a) < (b) ? (a) : (b))

#define SWAP(x, y, T) do { T t = x; x = y; y = t; } while (0)

#define S4BL_UNIT_NUM      ((S4BL_MODULUS_SIZE + __CHAR_BIT__ - 1)/ \
                            S4BL_WORD_NBITS) + 1

#define S4BL_HALF_SHIFT    (S4BL_WORD_NBITS/2)
#define LO_MASK           ((((S4BL_WORD)1) << (S4BL_HALF_SHIFT))-1)
#define LO_HALF(x)        ((x) & LO_MASK)
#define HI_HALF(x)        ((x) >> (S4BL_HALF_SHIFT))

#pragma message ("S4BL_WORD_SIZE is " XSTR(S4BL_WORD_SIZE) " bytes")

#ifndef S4BL_SUB_SKIP
#define S4BL_SUB_SKIP 0
#endif

/* a -= b, returns borrow */
#if S4BL_SUB_SKIP
unsigned int S4BL_sub(int n, S4BL_WORD *a, const S4BL_WORD *b)
{
    int i;
    unsigned int borrow = 0;
    for (i = 0; i < n; ++i)
    {
        S4BL_WORD subend = b[i] + borrow;
        if (subend >= borrow)
        {
            S4BL_WORD r = a[i] - subend;
            borrow = (r > a[i]) ? 1 : 0;
            a[i] = r;
        }
    }
    return borrow;
}
#else
unsigned int S4BL_sub(int n, S4BL_WORD *a, const S4BL_WORD *b)
{
    int i;
    unsigned int borrow = 0;
    for (i = 0; i < n; i++)
    {
        unsigned int borrow2 = (a[i] < borrow) ? 1 : 0;
        S4BL_WORD t = a[i] - borrow;
        borrow = ((t < b[i]) ? 1 : 0) + borrow2;
        a[i] = t - b[i];
    }

    return borrow;
}
#endif

int S4BL_cmp(int n, const S4BL_WORD *a, const S4BL_WORD* b)
{
    int i;
    for (i = n-1; i >=0; --i)
    {
        if ( a[i] > b[i]) return 1;
        if ( a[i] < b[i]) return -1;
    }
    return 0;
}


#if S4BL_WORD_SIZE==8 && defined(__SIZEOF_INT128__)
#pragma message "Using C 64 x 64 -> 128 S4BL_MULT_WORD and S4BL_SQR_WORD"
#define S4BL_MULT_WORD(u,v,a,b) {        \
     __uint128_t r = ((__uint128_t) a) * b; \
     u = (uint64_t) (r>>64);                \
     v = (uint64_t) r; }
#define S4BL_SQR_WORD(u,v,a) S4BL_MULT_WORD(u,v,a,a)
#endif

/*
 * default implementations
 */

#ifndef ADDC
#define ADDC(u,v,a)   v += a; u += (v<a)? 1: 0;
#endif

#ifndef S4BL_MULT_WORD
/* u,v = a * b */
#pragma message "Using default S4BL_MULT_WORD"
#define S4BL_MULT_WORD(u,v,a,b) {   \
S4BL_WORD al = LO_HALF(a);          \
S4BL_WORD ah = HI_HALF(a);          \
S4BL_WORD bl = LO_HALF(b);          \
S4BL_WORD bh = HI_HALF(b);          \
S4BL_WORD lh = al * bh;             \
S4BL_WORD hl = ah * bl;             \
v = al * bl;                        \
u = ah * bh;                        \
u += HI_HALF(lh);                   \
u += HI_HALF(hl);                   \
lh <<= S4BL_HALF_SHIFT;             \
hl <<= S4BL_HALF_SHIFT;             \
ADDC(u,v,lh);                       \
ADDC(u,v,hl); }
#endif

#ifndef S4BL_SQR_WORD
/* u,v = a * a */
#pragma message "Using default S4BL_SQR_WORD"
#define S4BL_SQR_WORD(u,v,a) {      \
S4BL_WORD al = LO_HALF(a);          \
S4BL_WORD ah = HI_HALF(a);          \
S4BL_WORD  m = al * ah;             \
          v = al * al;              \
          u = ah * ah;              \
u += HI_HALF(m);                    \
u += HI_HALF(m);                    \
m <<= S4BL_HALF_SHIFT;              \
ADDC(u,v,m);                        \
ADDC(u,v,m); }
#endif

#ifndef S4BL_MULT_ADD
/* r2:r1:r0 += (a * b) */
#pragma message "Using default S4BL_MULT_ADD"
#define S4BL_MULT_ADD(a,b,r0,r1,r2)  {  \
S4BL_WORD u, v;                         \
S4BL_MULT_WORD(u,v,a,b);                \
ADDC(r1,r0,v);                          \
ADDC(r2,r1,u); }
#endif

#ifndef S4BL_SQR_ADD1
#pragma message "Using default S4BL_SQR_ADD1"
#define S4BL_SQR_ADD1(a,r0,r1,r2)  {    \
S4BL_WORD u, v;                         \
S4BL_SQR_WORD(u,v,a);                   \
ADDC(r1,r0,v);                          \
ADDC(r2,r1,u); }
#endif

#ifndef S4BL_SQR_ADD2
#pragma message "Using default S4BL_SQR_ADD2"
#define S4BL_SQR_ADD2(a,b,r0,r1,r2)  {  \
S4BL_WORD u, v;                         \
S4BL_MULT_WORD(u,v,a,b);                \
ADDC(r1,r0,v);                          \
ADDC(r1,r0,v);                          \
ADDC(r2,r1,u);                          \
ADDC(r2,r1,u); }
#endif

/* multiplication: product scanning form */
void S4BL_mulEx(int n, S4BL_WORD* c, const S4BL_WORD* a, const S4BL_WORD *b,
               int last_index)
{
    int k;
    S4BL_WORD r0, r1, r2;
    r0 = r1 = r2 = 0;

    for (k = 0; k < last_index; ++k)
    {
        int i = MIN(k, n-1);
        int j, j_max = MIN(k, n-1);

        for (j = k - i; j <= j_max; ++j, --i)
        {
            /* r2:r1:r0 += a[i] * b[j]; */
            S4BL_MULT_ADD(a[i], b[j], r0, r1, r2);
        }
        c[k]    = r0;
        r0      = r1;
        r1      = r2;
        r2      = 0;
    }
    c[last_index] = r0;
    /* assert(r1 == 0); */
}

void S4BL_mul(int n, S4BL_WORD* c, const S4BL_WORD* a, const S4BL_WORD *b)
{
    S4BL_mulEx(n, c, a, b, 2*n-1);
}

/* square: product scanning form */
void S4BL_sqr(int n, S4BL_WORD* c, const S4BL_WORD* a)
{
    int k;
    S4BL_WORD r0, r1, r2;
    r0 = r1 = r2 = 0;
    for (k = 0; k < 2 * n - 1; ++k)
    {
        int i = MIN(k, n-1);
        int j;
        for (j = k - i; j < i; ++j, --i)
        {
            S4BL_SQR_ADD2(a[i], a[j], r0, r1, r2);
        }
        if (i == j)
        {
            S4BL_SQR_ADD1(a[i], r0, r1, r2);
        }
        c[k]    = r0;
        r0      = r1;
        r1      = r2;
        r2      = 0;
    }
    c[2 * n - 1] = r0;
    /* assert(r1 == 0); */
}


/* S4BL_barrett_reduction
*
* result[n], x[2n], modulus[n+1], mu[n+1], aux[2n+2]
*/
void S4BL_barrett_reduction(int n,
                           S4BL_WORD *x,
                           const S4BL_WORD *modulus,
                           const S4BL_WORD *mu,
                           S4BL_WORD* aux)
{
    /* result = x mod modulus using mu(modulus) as helper */

    /*
     q1 = floor(x / b^(n-1))
     q2 = q1 * mu
     q3 = floor(q2 / b^(n+1))
     r2 = (q3 * m) mod b^(n+1)
     r1 = x mod b^(n+1)
     r1  -=  r2
     if(r1 < 0)
     r1 = r1 + b^(n+1)
     while(r1 >= m)
     r1 = r1 - m
     return r1
     */

    int i;

    /* aux = floor(x / b^(n-1)) * mu  = q2 */
    S4BL_mul(n+1, aux, x + n - 1, mu);

    /* q3 = n+1 most significant units of q2*/

    /* r2 = (q3 * m) mod b^(n+1) */
    /* r2 = multiply n+1 most significant units of q2 by modulus
     * and keep only the n+1 least significant units -> aux
     */
    S4BL_mulEx(n+1, aux, aux + n + 1, modulus, n+1);

    /* r1 = x mod b^(n+1) - r2  -> x */
    S4BL_sub(n+1, x, aux);

    i = 0;
    while (S4BL_cmp( n+1, x, modulus) > 0)
    {
        S4BL_sub( n+1, x, modulus);
        if (++i >=2 )
        {
            extern void exit(int a);
            exit(-1);
        }
    }
    /* result in x */
}

void S4BL_barrett_multiplication(int n,
                                S4BL_WORD *c,
                                const S4BL_WORD *a,
                                const S4BL_WORD *b,
                                const S4BL_WORD *modulus,
                                const S4BL_WORD *mu,
                                S4BL_WORD *aux)
{
    S4BL_mul(n, c, a, b);
    S4BL_barrett_reduction(n, c, modulus, mu, aux);
}


void S4BL_barrett_sqr(int n,
                     S4BL_WORD *c,
                     const S4BL_WORD *a,
                     const S4BL_WORD *modulus,
                     const S4BL_WORD *mu,
                     S4BL_WORD *aux)
{
    S4BL_sqr(n, c, a);
    S4BL_barrett_reduction(n, c, modulus, mu, aux);
}


/* modular exponentiation barrett's method (c = x ^ e mod n) */
const S4BL_WORD* S4BL_barrett_modexp(int num_words, const S4BL_WORD *x,
                                     unsigned int e,
                                     const S4BL_WORD *modulus, const S4BL_WORD *mu,
                                     S4BL_WORD *aux)
{
    int i, first_bit_idx = ((int) sizeof(int) * 8) - __builtin_clz(e);

    S4BL_WORD *operand = aux;
    S4BL_WORD *result = operand + 2 * num_words;
    aux = result + 2 * num_words;

    for (i = 0; i < num_words; ++i)
    {
        operand[i] = x[i];
    }

    for (i = first_bit_idx - 2; i >= 0; --i)
    {
        S4BL_barrett_sqr(num_words, result, operand, modulus, mu, aux);

        SWAP(operand, result, S4BL_WORD*);

        if ((e >> i) & 1)
        {
            S4BL_barrett_multiplication(num_words, result, operand, x,
                                       modulus, mu, aux);
            SWAP(operand, result, S4BL_WORD*);
        }
    }
    return operand;
}
