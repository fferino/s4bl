#
#  x86_64.mk
#
#  s4bl: simple secure boot loader
#
#  Copyright 2024 Fabrice Ferino
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


ifneq ($(TARGET),$(UNAME_M))

CROSS_COMPILE:=x86_64-linux-gnu
EXEC_PREFIX:=qemu-x86_64-static

endif

CPPFLAGS+=-DS4BL_WORD_SIZE=8
