/*
  s4bl
 
 S4BL: a simple secure boot loader framework

 Copyright 2024 Fabrice Ferino

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */


#ifndef s4bl_h
#define s4bl_h

#ifdef __cplusplus
extern "C" {
#endif

#include "s4bl_big_int.h"

/* C89 does not support bool types: define BOOL type here to prevent
 * ambiguity about interpretation of the return value
 */
typedef enum {
    S4BL_FALSE = 0,
    S4BL_TRUE
} S4BL_BOOL;


typedef enum
{
    S4BL_SHA256 = 1,
    S4BL_SHA384 = 2,
    S4BL_SHA512 = 3,
    S4BL_SHA224 = 4,
    S4BL_SHA512_224 = 5,
    S4BL_SHA512_256 = 6,
    S4BL_DIGEST_ALGO_LAST
} S4BL_digest_algo_t;

/*
 S4BL main entry point to verify signature
 */
S4BL_BOOL S4BL_verify_signature(S4BL_digest_algo_t algo,
                                const unsigned char *digest,
                                const S4BL_WORD *signature);

#ifdef __cplusplus
} // extern "C"
#endif


#endif /* s4bl_h */
