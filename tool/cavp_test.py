#!/usr/bin/env python3
#
# Copyright 2024 Fabrice Ferino
# SPDX-License-Identifier: Apache-2.0
#

'''
parses FIPS CAVP old style test vector (non JSON, non ACVP)
and emits binary data:
big endian 2 bytes: length of message,
bytes: message,
bytes: digest

the resulting stream can be processed by the implementer
or saved to a file

'''

import sys
import binascii

def emit_bytes_from_hex(hex_str: bytes) -> None:
    ''' convert hex string to bytes and write them to stdout '''
    sys.stdout.buffer.write(binascii.a2b_hex(hex_str.rstrip()))

for line in sys.stdin:

    if line.startswith('Len = '):
        msg_size = int(line[6:], 0)
        msg_size //= 8  # bits to bytes
        msg_bytes = msg_size.to_bytes(2, byteorder='big')
        sys.stdout.buffer.write(msg_bytes)
    elif line.startswith('Msg = ') and msg_size > 0:
        emit_bytes_from_hex(line[6:])
    elif line.startswith('MD = '):
        emit_bytes_from_hex(line[5:])
    elif line.startswith('[L = '):
        dgst_size = int(line[5:line.find(']')], 0)
        msg_bytes = dgst_size.to_bytes(2, byteorder='big')
        sys.stdout.buffer.write(msg_bytes)
    else:
        continue

sys.stdout.flush()
