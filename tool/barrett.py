#! /usr/bin/env python3
#
# barrett.py
#
#  s4bl: simple secure boot loader
#
#  Copyright 2024 Fabrice Ferino
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

'''
 This module provides routines to extract the public key
 from PEM RSA files and to compute the barret Mu quantity
 for a RSA key
'''

# pylint: disable=consider-using-f-string

import re
import getpass

from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa


PEM_RE = re.compile(rb'----( |-)BEGIN ([A-Z0-9 ]+)\1----')


def pem_find(pem_data: bytes) -> (bytes, int):
    ''' detect start of PEM data in file '''
    m = PEM_RE.search(pem_data)
    if m:
        return m.group(2), m.start()

    return None, -1


def private_key_from_pem(pem_data: bytes) -> rsa.RSAPrivateKey:
    ''' return RSA private key from PEM data of private key '''
    private_key = None
    try:
        private_key = serialization.load_pem_private_key(pem_data,
                                                         password=None)
    except TypeError as _:
        pass

    if not private_key:
        password_str = getpass.getpass("Enter private key file password: ")
        password = password_str.encode('utf-8')
        private_key = serialization.load_pem_private_key(pem_data,
                                                         password=password)

    if not isinstance(private_key, rsa.RSAPrivateKey):
        raise ValueError("Not an RSA key")
    return private_key

def pub_key_from_private_key(pem_data: bytes) -> rsa.RSAPublicNumbers:
    ''' return RSA public numbers from PEM data of private key '''
    private_key = private_key_from_pem(pem_data)
    return private_key.public_key().public_numbers()


def pub_key_from_rsa_public_key(pem_data: bytes) -> rsa.RSAPublicNumbers:
    ''' return RSA public numbers from PEM data of public key '''
    public_key = serialization.load_pem_public_key(pem_data)

    if not isinstance(public_key, rsa.RSAPublicKey):
        raise ValueError("Not an RSA key")

    return public_key.public_numbers()


def pub_key_from_key_file(key_file: str) -> rsa.RSAPublicNumbers:
    ''' return the pub_key of an RSA key from a file (PEM or DER) '''
    with open(key_file, "rb") as f:
        key_data = f.read()

    pem_type, pem_start = pem_find(key_data)

    if not pem_type:
        raise ValueError("No PEM block in file %s " %
                         key_file)

    pem_data = key_data[pem_start:]
    if pem_type == b'RSA PRIVATE KEY':
        return pub_key_from_private_key(pem_data)

    if pem_type in (b'RSA PUBLIC KEY', b'PUBLIC KEY'):
        return pub_key_from_rsa_public_key(pem_data)

    raise ValueError("PEM Type %s not supported" %
                     pem_type.decode('ascii', 'backslashreplace'))


def compute_barrett_mu(m: int, base_bits: int = 32) -> int:
    ''' compute barrett's mu for the modulus m '''
    b = 2 ** base_bits
    k = (m.bit_length() + base_bits - 1 ) // base_bits
    u = (b ** (2*k)) // m
    return u
