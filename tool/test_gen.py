#!/usr/bin/env python3
#
#  test_gen.py
#
#  s4bl: simple secure boot loader
#
#  Copyright 2024 Fabrice Ferino
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

'''
 Test generator: generate test input: modexp or verification tests,
 negative or positive tests.
'''

# pylint: disable=consider-using-f-string

import binascii
import argparse
import random

from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding, utils

from barrett import (pem_find,
                     private_key_from_pem,
                     compute_barrett_mu,
                     rsa)

HASHES = (
    hashes.SHA224,
    hashes.SHA256,
    hashes.SHA384,
    hashes.SHA512,
    hashes.SHA512_224,
    hashes.SHA512_256
)

def int2bytes(x:int) -> bytes:
    ''' convert integer to bytes '''
    return x.to_bytes((x.bit_length() + 7) // 8, byteorder='big')

def int2hex(x:int) -> str:
    ''' convert integer to hex bytes padding to even number of chars '''
    h = hex(x)[2:]
    if len(h) & 1:
        h = '0' + h
    return h

def change_bytes(some_bytes:bytes, doit: bool) -> bytes:
    '''
    conditionally randomly change one byte in the sequence passed as argument;
    used to generate negative tests input
    '''
    if doit:
        index = random.randrange(len(some_bytes))
        some_bytes = bytearray(some_bytes)
        some_bytes[index] ^= 1
    return some_bytes

def change_hash_class(orig_class: hashes.HashAlgorithm,
                      doit: bool) -> hashes.HashAlgorithm :
    '''
    conditonally change the hashing algorithm passed as argument;
    used to generate negative tests input
    '''
    if not doit:
        return orig_class

    new_class = random.choice(HASHES)
    while new_class == orig_class:
        new_class = random.choice(HASHES)
    return new_class


def read_private_key(key_file: str) -> rsa.RSAPrivateKey:
    ''' read a private key from key_file '''
    with open(key_file, "rb") as f:
        key_data = f.read()

    pem_type, pem_start = pem_find(key_data)

    if not pem_type:
        raise ValueError("No PEM block in file %s " %
                         key_file)

    pem_data = key_data[pem_start:]
    
    return private_key_from_pem(pem_data)

def generate_digest_signature(private_key: rsa.RSAPrivateKey,
                              hash_class: hashes.HashAlgorithm,
                              pss_padding) -> (bytes, bytes):
    ''' generate a random digest of the specified hash algorithm and
    sign it with the private key; returns digest and signature '''
    digest_algo = hash_class()

    digest = random.randbytes(digest_algo.digest_size)

    if pss_padding:
        padder = padding.PSS(mgf=padding.MGF1(digest_algo),
                                 salt_length=padding.PSS.MAX_LENGTH)
    else:
        padder = padding.PKCS1v15()

    signature = private_key.sign(digest,
                                 padder,
                                 utils.Prehashed(digest_algo))
    return digest, signature


FAILURE_TYPES_COMMON=("signature", )
FAILURE_TYPES_MODEXP= FAILURE_TYPES_COMMON + ("payload", )
FAILURE_TYPES_VERIF= FAILURE_TYPES_COMMON + ("padding", "hash_algo", "digest")

def generate_test(key_file: str,
                  word_size_bits: int,
                  num_tests: int,
                  test_type: str,
                  failing: bool) -> None:
    '''
    generate and print test input to standard output
    '''

    private_key = read_private_key(key_file)
    pub_numbers = private_key.public_key().public_numbers()

    mu = compute_barrett_mu(pub_numbers.n, word_size_bits)

    modexp_test = test_type=="modexp"
    failures = FAILURE_TYPES_MODEXP if modexp_test else FAILURE_TYPES_VERIF

    for _ in range(num_tests):

        failure = random.choice(failures) if failing else ""

        hash_class = random.choice(HASHES)
        digest,signature = generate_digest_signature(private_key,
                                                     hash_class,
                                                     failure=="padding")

        print(test_type + failure)
        print(binascii.b2a_hex(change_bytes(signature,
                                            failure== "signature")
                               ).decode('ascii'))
        print(int2hex(pub_numbers.e))
        print(int2hex(pub_numbers.n))
        print(int2hex(mu))

        if modexp_test:
            payload = private_key.public_key().recover_data_from_signature(
                signature,
                padding.PKCS1v15(),
                None)
            print(binascii.b2a_hex(change_bytes(payload,
                                                failure=="payload")
                                   ).decode('ascii'))
        else:
            print(change_hash_class(hash_class,
                                    failure=="hash_algo"
                                    ).name.replace('-', '_').upper())
            print(binascii.b2a_hex(change_bytes(digest,
                                                failure=="digest")
                                   ).decode('ascii'))

def main():
    ''' main function '''
    parser = argparse.ArgumentParser()
    parser.add_argument('-b', '--bits-in-word', default='32',
                        type=lambda x: int(x,0),
                        help="Number of bits in word (default: %(default)s)")
    parser.add_argument('-f', '--failing', action='store_true',
                        help="Generate failing (or negative) tests")
    parser.add_argument('-n', '--num-tests', default='1',
                        type=lambda x: int(x,0),
                        help="Number of tests per file (default: %(default)s)")
    parser.add_argument('-t', '--test-type', default='verify',
                        choices=('verify', 'modexp'),
                        help="Test type (default: %(default)s)")
    parser.add_argument('key_file', nargs='+', metavar='key.pem',
                        help="Private key file to use")
    args = parser.parse_args()
    for f in args.key_file:
        generate_test(f,
                      args.bits_in_word,
                      args.num_tests,
                      args.test_type,
                      args.failing)


if __name__ == "__main__":
    main()
