# s4bl

s4bl is a framework to develop secure boot loaders.

The goals of s4bl is to make it easy or even possible to implement secure boot
loaders on any platform by 
1. providing ROM-able code to verify signatures 
2. simple tools to generate the signatures to be used by the code above

In particular the ROM-able code must be secure, small, simple to integrate, and
with adequate performance so that it can be used for as many projects as 
possible.
The code should have a small, fixed memory footprint with no runtime memory allocation.

## Usage
TBD: there will be a tool to 
1. generate the code (proper optimizations for the CPU/ISA) 
1. write keys in the optimal format for the CPU/ISA (S4BL big integer format)
1. generate signatures in the optimal format for the CPU/ISA (S4BL big integer format)

## Support

Please use the Gitlab Issues system to report issues.

## Roadmap

* Add more CPU specific configurations.
* Get performance numbers: code size, stack usage, latency.
* Develop the tool described above.

## Contributing

The project is still being actively developed. 

## License
[Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

